
-- Insert Data to Users Table
Insert Into users (email, password, dateTime_created) values ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00"), ("juandelacrux@gmail.com", "passwordB", "2021-01-01 02:00:00"), ("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00"), ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00"), ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");


-- Insert Data to Posts Table
Insert Into posts (user_id, title, content, dateTime_posted) values (1, "First Code", "Hello World!", "2021-01-02 01:00:00"), (1, "Second Code", "Hello Earth!", "2021-01-02 02:00:00"), (2, "Third Code", "Welcome to Mars!", "2021-01-02 03:00:00"), (4, "Fourth Code", "Bye bye solar system!", "2021-01-02 04:00:00");

-- Get all the post with an Author ID of 1.

Select * From posts where author_id = "1";

-- Get all the user's email and datetime of creation.

Select email, dateTime_created From users;

-- Update a post's content to "Hello to the people of the Earth!” where its initial content is "Hello Earth!" by using the record's ID.

Update posts Set content = "Hello people of the Earth!" Where content = "Hello Earth!";

-- Delete the user with an email of "johndoe@gmail.com".

Delete from users where email = "johndoe@gmail.com";